    list P = PIC12F752, r=dec
    #include "p12F752.inc"
    __config(_CP_OFF & _WDTE_OFF & _PWRTE_ON & _FOSC0_INT)    

;
; definitions
;
    
Counter equ 0x40
ShiftRegister equ 0x41
XMIT_Flag equ 0x42
REC_Flag equ 0x43

    org 0
    goto Main
    org 5
 
Main:
    call Setup ;Setup stuff
    movlw 0
    
MainLoop:    
    
    call StartTransmit
    addlw 1 ; increment W to change the val transmitted
    goto MainLoop
    

 
Setup: ;Set up HLTMR1, PortA0, and interrupts
    ;Init Ports
    banksel ANSELA
    bcf ANSELA,1 ; Setting RA1,4 to be digital 
    bcf ANSELA,4
    banksel LATA ; Set output line HI right away;    bsf LATA,1
    banksel TRISA ; Set Pin 1 to be output, Pin 4 input
    bcf TRISA,1
    bsf TRISA,4
    ;Init  H1TMR1
    banksel HLTPR1
    movlw 206 ; Inserting timer period value for baud rate
    movwf HLTPR1
    movlw 0x01 ; Set Post scale 4, Timer off, Prescale 1
    movwf HLT1CON0
    movlw b'00011000' ;Reserved input source, no effect of RE or FE
    movwf HLT1CON1
    ;Init Interrupts
    banksel INTCON ; Enabling interrupts
    bsf INTCON,GIE ;global int set
    bsf INTCON,PEIE ;peripheral int set
    banksel PIE1
    bsf PIE1,HLTMR1IE ;Enables Hardware timer int
    
    ;Clear flags now
    banksel XMIT_Flag 
    clrf XMIT_Flag
    clrf REC_Flag
    return

StartTransmit:
    banksel XMIT_Flag
    btfss XMIT_Flag,0 ;If still transmitting, don't transmit new byte
    goto MainLoop
    
    movwf ShiftRegister ;Copy xmit byte to SR
    movlw 10 ; Set counter to 10
    movwf Counter
    bsf XMIT_Flag,0 ;Set Transmit flag
    
    banksel HLT1CON0 ; Turn on hardware timer (interrupts will fire)
    bsf HLT1CON0,H1ON 
    banksel LATA ; Set XMIT line LO (Start bit)
    bcf LATA,1
    banksel PIR1
    clrf PIR1 ; clear pend local interrupts (inc. HLTMR1F)
    
    return
    
    end